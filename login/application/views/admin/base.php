<?php
if ($this->login->check_session() == FALSE) {
    header('HTTP/1.0 404 Not Found', TRUE, 404);
    exit('Page Not Found !');
}
?>
<!DOCTYPE html>
<html lang="en" class="gr__jthemes_org">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta name="author" content="Exolim"/>
    <meta name="robots" content="noindex, nofollow">
    <title>Management Dashboard | <?php echo config_item('company_name') ?></title>
    <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"
          rel="stylesheet"
          type="text/css"/>
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
          rel="stylesheet"
          type="text/css"/>
    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="<?php echo base_url('axxets/admin/theme.css') ?>"
          rel="stylesheet"
          type="text/css"/>
    <link rel="stylesheet"
          href="//code.jquery.com/ui/1.12.1/themes/eggplant/jquery-ui.css">
    <link rel="shortcut icon"
          href="<?php echo base_url('uploads/favicon.ico') ?>"/>

    <script src="//code.jquery.com/jquery-3.4.1.min.js"
            type="text/javascript"></script>
    <script type="text/javascript"
            src="//www.gstatic.com/charts/loader.js"></script>
    <?php
    if (!isset($this->session->designation)) {
        $payout      = $this->db_model->sum('amount', 'earning');
        $paid_payout = $this->db_model->sum('amount', 'withdraw_request', array('status' => 'Paid'));
        if ($paid_payout == "") {
            $paid_payout = 0;
        }
        $pending_payout = $this->db_model->sum('amount', 'withdraw_request', array('status' => 'Pending'));
        if ($pending_payout == "") {
            $pending_payout = 0;
        }
        $earnnings = $this->db_model->sum('topup', 'member');
    }
    ?>
    <script type="text/javascript">
        google.charts.load("current", {packages: ["corechart"]});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Income Name', 'Amount'],
                ['Earned', <?php echo $earnnings ?>],
                ['Paid Payout', <?php echo $paid_payout ?>],
                ['Pending Payout', <?php echo $pending_payout ?>],
            ]);

            var options = {
                title: 'Total Business Activity',
                pieHole: 0.4,
            };

            var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
            chart.draw(data, options);
        }
    </script>
</head>
<body class="" data-gr-c-s-loaded="true" cz-shortcut-listen="true">
<!-- ===== Main-Wrapper ===== -->
<div id="wrapper">
    <nav class="navbar navbar-default navbar-static-top m-b-0">
        <div class="navbar-header">
            <a class="navbar-toggle font-20 hidden-sm hidden-md hidden-lg " href="javascript:void(0)"
               data-toggle="collapse" data-target=".navbar-collapse">
                <i class="fa fa-bars"></i>
            </a>
            <div class="top-left-part">
                <a class="logo" href="<?php echo site_url('admin') ?>">

                </a>
            </div>
            <ul class="nav navbar-top-links navbar-left hidden-xs">
                <li>
                    <a href="javascript:void(0)" class="sidebartoggler font-20 waves-effect waves-light"><i
                                class="icon-arrow-left-circle"></i></a>
                </li>
                <li>
                    <form role="search" class="app-search hidden-xs" action="<?php echo site_url('users/search') ?>"
                          method="POST">
                        <i class="icon-magnifier"></i>
                        <input type="text" name="userid" placeholder="Search User.." class="form-control">
                    </form>
                </li>
            </ul>
            <ul class="nav navbar-top-links navbar-right pull-right">
                <li class="right-side-toggle">
                    <a class="right-side-toggler waves-effect waves-light b-r-0 font-20"
                       href="<?php echo site_url('admin/setting') ?>">
                        <i class="icon-settings"></i>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- ===== Top-Navigation-End ===== -->
    <!-- ===== Left-Sidebar ===== -->
    <aside class="sidebar">
        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;">
            <div class="scroll-sidebar" style="overflow: hidden; width: auto; height: 100%;">
                <div class="user-profile">
                    <div class="dropdown user-pro-body">
                        <div class="profile-image">
                            <img src="<?php echo base_url('uploads/logo.png') ?>" class="logo"
                                 style="max-width: 200px; height: auto" alt="home">
                        </div>
                        <p class="profile-text m-t-15 font-16"><a
                                    href="javascript:void(0);"><?php echo $this->session->name ?></a></p>
                    </div>
                </div>
                <nav class="sidebar-nav active">
                    <ul id="side-menu" class="in">
                        <li class="active">
                            <a class="active waves-effect" href="<?php echo site_url('admin') ?>" aria-expanded="false"><i
                                        class="icon-screen-desktop fa-fw"></i> <span
                                        class="hide-menu"> Dashboard </span></a></li>

                        <?php if (!isset($this->session->designation) || $this->session->designation['b_setting'] == "1") { ?>
                            <li>
                                <a href="#">
                                    <i class="fa fa-briefcase fa-fw"></i>
                                    <span class="title">Business Settings</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="<?php echo site_url('setting/common-setting') ?>"
                                           class="nav-link ">
                                            <span class="title">General Settings</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('setting/advance-setting') ?>"
                                           class="nav-link ">
                                            <span class="title">Business Settings</span>
                                        </a>
                                    </li>
                                    <?php if (config_item('enable_help_plan') !== "Yes") { ?>
                                        <li>
                                            <a href="<?php echo site_url('setting/payout-setting') ?>"
                                               class="nav-link ">
                                                <span class="title">Payout Setting</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('income/flexible-income') ?>"
                                               class="nav-link ">
                                                <span class="title">Flexible Income Setting</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('income/set-level-wise') ?>"
                                               class="nav-link ">
                                                <span class="title">Level Wise Income Setting</span>
                                            </a>
                                        </li>
                                    <?php } ?>
                                    <li>
                                        <a href="<?php echo site_url('setting/rank-setting') ?>"
                                           class="nav-link ">
                                            <span class="title">Rank Setting</span>
                                        </a>
                                    </li>
                                    <?php if (config_item('enable_help_plan') !== "Yes") { ?>
                                        <?php if (config_item('fix_income') == 'Yes') { ?>
                                            <li>
                                                <a href="<?php echo site_url('income/income-setting') ?>"
                                                   class="nav-link ">
                                                    <span class="title">Fix Income Setting</span>
                                                </a>
                                            </li>
                                        <?php }
                                        if (config_item('enable_repurchase') == "Yes") { ?>
                                            <li>
                                                <a href="<?php echo site_url('income/gap-commission-setting') ?>"
                                                   class="nav-link ">
                                                    <span class="title">Repurchase Gap Commission Setting</span>
                                                </a>
                                            </li>
                                        <?php }
                                    } ?>
                                    <li>
                                        <a href="<?php echo site_url('setting/payment-gateway') ?>"
                                           class="nav-link ">
                                            <span class="title">Payment Gateways</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('setting/welcome-letter') ?>"
                                           class="nav-link ">
                                            <span class="title">Design Welcome Letter</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('setting/clear-database') ?>"
                                           class="nav-link ">
                                            <span class="title">Clear/Reset Database</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php if (!isset($this->session->designation) || $this->session->designation['user_manage'] == "1") { ?>
                            <li>
                                <a href="#">
                                    <i class="fa fa-users fa-fw"></i>
                                    <span class="hide-menu">Members
                                    <span class="label label-rounded label-info pull-right fa fa-arrow-right"></span>
                                        </span>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="<?php echo site_url('users/view-members') ?>"
                                           class="nav-link ">
                                            <span class="title">View/Manage Members</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('users/search_user') ?>"
                                           class="nav-link ">
                                            <span class="title">Search Members</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('users/blocked-members') ?>"
                                           class="nav-link ">
                                            <span class="title">Blocked Members</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('users/latest-members') ?>"
                                           class="nav-link ">
                                            <span class="title">Latest Members</span>
                                        </a>
                                    </li>
                                    <?php if (config_item('enable_help_plan') !== "Yes") { ?>
                                        <?php if (config_item('enable_topup') == "Yes") { ?>
                                            <li>
                                                <a href="<?php echo site_url('users/topup-member') ?>"
                                                   class="nav-link ">
                                                    <span class="title">TopUp Member</span>
                                                </a>
                                            </li>
                                        <?php }
                                    } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php if (!isset($this->session->designation) || $this->session->designation['tree_view'] == "1") { ?>
                            <li>
                                <a href="javascript:void;" class="waves-effect">
                                    <i class="fa fa-sitemap fa-fw"></i>
                                    <span class="title">User Tree</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a target="_blank" href="<?php echo site_url('tree/add-new') ?>"
                                           class="nav-link ">
                                            <span class="title">Add New Member</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('tree/user-tree') ?>"
                                           class="nav-link ">
                                            <span class="title">User Tree</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('tree/downline-report') ?>"
                                           class="nav-link ">
                                            <span class="title">Downline Report</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('tree/referred-list') ?>"
                                           class="nav-link ">
                                            <span class="title">Referred Members</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php if (config_item('enable_help_plan') !== "Yes") { ?>
                            <?php if (!isset($this->session->designation) || $this->session->designation['wallet'] == "1") { ?>
                                <li>
                                    <a href="javascript:void;" class="waves-effect">
                                        <i class="fa fa-cc-visa fa-fw"></i>
                                        <span class="title">E-Wallet</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li>
                                            <a href="<?php echo site_url('wallet/manage-wallet-fund') ?>"
                                               class="nav-link ">
                                                <span class="title">Manage Wallet Fund</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('wallet/transfer-fund') ?>"
                                               class="nav-link ">
                                                <span class="title">Transfer Fund</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('wallet/wallet-transactions') ?>"
                                               class="nav-link ">
                                                <span class="title">Wallet Transactions</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('wallet/withdraw-fund') ?>"
                                               class="nav-link ">
                                                <span class="title">Withdraw Fund</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('wallet/withdrawl-report') ?>"
                                               class="nav-link ">
                                                <span class="title">Withdrawal Report</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <?php
                            }
                        }
                        ?>
                        <?php if ((!isset($this->session->designation) || $this->session->designation['epin'] == "1") && config_item('enable_epin') == "Yes") { ?>

                            <li>
                                <a href="javascript:void;" class="waves-effect">
                                    <i class="fa fa-xing-square fa-fw"></i>
                                    <span class="title">e-PIN</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="<?php echo site_url('admin/generate_epin') ?>"
                                           class="nav-link ">
                                            <span class="title">Generate e-PIN</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('admin/unused_epin') ?>"
                                           class="nav-link ">
                                            <span class="title">Un-Used e-PINs</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('admin/used_epin') ?>"
                                           class="nav-link ">
                                            <span class="title">Used e-PINs</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('admin/search_epin') ?>"
                                           class="nav-link ">
                                            <span class="title">Search e-PIN</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('admin/transfer_epin') ?>"
                                           class="nav-link ">
                                            <span class="title">Transfer e-PINs</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <?php
                        }
                        ?>
                        <?php if (config_item('enable_help_plan') !== "Yes") { ?>
                            <?php if (!isset($this->session->designation) || $this->session->designation['earning_manage'] == "1") { ?>
                                <li>
                                    <a href="javascript:void;" class="waves-effect">
                                        <i class="fa fa-money fa-fw"></i>
                                        <span class="title">Earning & Payout</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li>
                                            <a href="<?php echo site_url('income/view-earning') ?>"
                                               class="nav-link ">
                                                <span class="title">View Earnings</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('income/search-earning') ?>"
                                               class="nav-link ">
                                                <span class="title">Search Earnings</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('income/make-payment') ?>"
                                               class="nav-link ">
                                                <span class="title">Make Payment</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('income/hold-payments') ?>"
                                               class="nav-link ">
                                                <span class="title">Hold Payments</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('income/search-payout') ?>"
                                               class="nav-link ">
                                                <span class="title">Search Payout</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('income/tax-report') ?>"
                                               class="nav-link ">
                                                <span class="title">Tax Report</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('cron/generate_payout') ?>"
                                               class="nav-link ">
                                                <span class="title">Generate Payout</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <?php if (config_item('enable_reward') == "Yes") { ?>
                                    <li>
                                        <a href="javascript:void;" class="waves-effect">
                                            <i class="fa fa-gift fa-fw"></i>
                                            <span class="title">Rewards</span>
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="<?php echo site_url('income/pay-rewards') ?>"
                                                   class="nav-link ">
                                                    <span class="title">Pay Rewards</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('income/reward-search-form') ?>"
                                                   class="nav-link ">
                                                    <span class="title">Search Rewards</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('setting/reward-setting') ?>"
                                                   class="nav-link ">
                                                    <span class="title">Reward Setting</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                <?php }
                            }
                        } ?>

                        <?php if (config_item('enable_investment') === "Yes") { ?>
                            <li>
                                <a href="javascript:void;" class="waves-effect">
                                    <i class="fa fa-bitcoin fa-fw"></i>
                                    <span class="title">Investments</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="<?php echo site_url('investments/create_pack') ?>"
                                           class="nav-link ">
                                            <span class="title">Create a New Pack</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('investments/manage_packs') ?>"
                                           class="nav-link ">
                                            <span class="title">Manage Packages</span>
                                        </a>
                                    </li>
                                    <?php if (config_item('investment_mode') == "MANUAL") { ?>
                                        <li>
                                            <a href="<?php echo site_url('investments/approve_investments') ?>"
                                               class="nav-link ">
                                                <span class="title">Approve Investments</span>
                                            </a>
                                        </li>
                                    <?php } ?>
                                    <li>
                                        <a href="<?php echo site_url('investments/manage_investments') ?>"
                                           class="nav-link ">
                                            <span class="title">Recent Investments</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('investments/search_investments') ?>"
                                           class="nav-link ">
                                            <span class="title">Search Investments</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php if (config_item('enable_help_plan') === "Yes") { ?>
                            <li>
                                <a href="javascript:void;" class="waves-effect">
                                    <i class="fa fa-gift fa-fw"></i>
                                    <span class="title">Manage Donations</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="<?php echo site_url('donation/create_pack') ?>"
                                           class="nav-link ">
                                            <span class="title">Create a New Pack</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('donation/manage_packs') ?>"
                                           class="nav-link ">
                                            <span class="title">Manage Packages</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('donation/manage_donations') ?>"
                                           class="nav-link ">
                                            <span class="title">Manage Donations</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('donation/upgrade_level') ?>"
                                           class="nav-link ">
                                            <span class="title">Upgrade Levels</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('donation/search_donations') ?>"
                                           class="nav-link ">
                                            <span class="title">Search Donations</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php if ((!isset($this->session->designation) || $this->session->designation['manage_poducts'] == "1") && config_item('enable_product') == "Yes") { ?>
                            <li>
                                </a>
                                <ul class="sub-menu">
                                    <?php if (!isset($this->session->designation) || $this->session->designation['manage_poducts'] == "1" || $this->session->designation['view_orders'] == "1") { ?>
                                        <li>
                                        </li>
                                    <?php } ?>
                                    <?php if (!isset($this->session->designation) || $this->session->designation['view_orders'] == "1") { ?>
                                        <li>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php if ((!isset($this->session->designation) || $this->session->designation['coupon'] == "1") && config_item('enable_coupon') == "Yes") { ?>
                            <li>
                                <a href="javascript:void;" class="waves-effect">
                                    <i class="fa fa-tag fa-fw"></i>
                                    <span class="title">Coupon Management</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="<?php echo site_url('coupon/manage-cat') ?>"
                                           class="nav-link ">
                                            <span class="title">Manage Categories</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('coupon/create-coupon') ?>"
                                           class="nav-link ">
                                            <span class="title">Generate Coupons</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('coupon/manage-coupons') ?>"
                                           class="nav-link ">
                                            <span class="title">Manage Coupons</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('coupon/use-requests') ?>"
                                           class="nav-link ">
                                            <span class="title">Coupon Use Requests</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php if (!isset($this->session->designation) || $this->session->designation['staff'] == "1") { ?>
                            <li>
                                <a href="javascript:void;" class="waves-effect">
                                    <i class="fa fa-user-secret fa-fw"></i>
                                    <span class="title">Manage Staff</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="<?php echo site_url('staff/designations') ?>"
                                           class="nav-link ">
                                            <span class="title">Manage Designations</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('staff/new-staff') ?>"
                                           class="nav-link ">
                                            <span class="title">Add New Staff</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('staff/list-staffs') ?>"
                                           class="nav-link ">
                                            <span class="title">Manage Staffs</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('staff/pay-salary') ?>"
                                           class="nav-link ">
                                            <span class="title">Pay Salary</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('staff/salary-report') ?>"
                                           class="nav-link ">
                                            <span class="title">Salary Report</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php if (config_item('enable_help_plan') !== "Yes") { ?>
                            <?php if (!isset($this->session->designation) || $this->session->designation['franchisee'] == "1") { ?>
                                <li>
                                    <a href="javascript:void;" class="waves-effect">
                                        <i class="fa fa-wpbeginner fa-fw"></i>
                                        <span class="title">Manage Franchisee</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li>
                                            <a href="<?php echo site_url('adm-franchisee/add-fran') ?>"
                                               class="nav-link ">
                                                <span class="title">New Franchisee</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('adm-franchisee/stock-management') ?>"
                                               class="nav-link ">
                                                <span class="title">Franchisee Stock Management</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('adm-franchisee/manage-fran') ?>"
                                               class="nav-link ">
                                                <span class="title">Manage Franchisee</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            <?php }
                        } ?>
                        <?php if (!isset($this->session->designation) || $this->session->designation['support'] == "1") { ?>
                            <li>
                                <a href="javascript:void;" class="waves-effect">
                                    <i class="fa fa-envelope fa-fw"></i>
                                    <span class="title">Support Tickets</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="<?php echo site_url('ticket/unsolved') ?>"
                                           class="nav-link ">
                                            <span class="title">Unsolved Tickets</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('ticket/resolved') ?>"
                                           class="nav-link ">
                                            <span class="title">Resolved Tickets</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php if (!isset($this->session->designation) && config_item('enable_ad_incm') == "Yes") { ?>
                            <li>
                                <a href="javascript:void;" class="waves-effect">
                                    <i class="fa fa-bullhorn fa-fw"></i>
                                    <span class="title">Advt Income</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="<?php echo site_url('ads/manage_ads') ?>"
                                           class="nav-link ">
                                            <span class="title">Manage Ads</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('ads/achievers') ?>"
                                           class="nav-link ">
                                            <span class="title">Ad Browsers</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php if (!isset($this->session->designation) && config_item('enable_survey') == "Yes") { ?>
                            <li>
                                <a href="javascript:void;" class="waves-effect">
                                    <i class="fa fa-list fa-fw"></i>
                                    <span class="title">Survey Master</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="<?php echo site_url('survey/manage_survey') ?>"
                                           class="nav-link ">
                                            <span class="title">Manage Surveys</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('survey/survey_report') ?>"
                                           class="nav-link ">
                                            <span class="title">Survey Reports</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <?php } ?>

                        <?php if (!isset($this->session->designation) && config_item('enable_recharge') == "Yes") { ?>
                            <li>
                                <a href="javascript:void;" class="waves-effect">
                                    <i class="fa fa-mobile-phone fa-fw"></i>
                                    <span class="title">Recharge Portal</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="<?php echo site_url('recharge/records') ?>"
                                           class="nav-link ">
                                            <span class="title">Recharge Records</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php if (config_item('enable_help_plan') !== "Yes") { ?>
                            <?php if (!isset($this->session->designation) || $this->session->designation['invoice'] == "1") { ?>
                                <li>
                                    <a href="javascript:void;" class="waves-effect">
                                        <i class="fa fa-print fa-fw"></i>
                                        <span class="title">Accounting & Invoice</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li>
                                            <a href="<?php echo site_url('accounting/invoices') ?>"
                                               class="nav-link ">
                                                <span class="title">Invoices</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('accounting/purchase') ?>"
                                               class="nav-link ">
                                                <span class="title">Purchases</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('accounting/accounting') ?>"
                                               class="nav-link ">
                                                <span class="title">P/L Sheet</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('accounting/transactionlogs') ?>"
                                               class="nav-link ">
                                                <span class="title">Transaction Logs</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            <?php }
                        } ?>
                        <?php if (!isset($this->session->designation) || $this->session->designation['expense'] == "1") { ?>

                            <li>
                                <a href="javascript:void;" class="waves-effect">
                                    <i class="fa fa-usd fa-fw"></i>
                                    <span class="title">Manage Expenses</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="<?php echo site_url('admin/expense') ?>"
                                           class="nav-link ">
                                            <span class="title">Manage Expenses</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <?php } ?>

                    </ul>
                </nav>
                <div class="p-30">
                    <a href="<?php echo site_url('admin/setting') ?>" target="_blank"
                       class="btn btn-success">Profile</a><br/><br/>
                    <a href="<?php echo site_url('admin/logout') ?>" target="_blank"
                       class="btn btn-primary">Logout</a>
                </div>
            </div>
            <div class="slimScrollBar"
                 style="background: rgb(220, 220, 220) none repeat scroll 0% 0%; width: 5px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 2764px;"></div>
            <div class="slimScrollRail"
                 style="width: 5px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
        </div>
    </aside>
    <!-- ===== Left-Sidebar-End ===== -->
    <!-- ===== Page-Content ===== -->
    <div class="page-wrapper" style="min-height: 499px;">
        <!-- ===== Page-Container ===== -->
        <div class="container-fluid">
            <?php
            echo validation_errors('<div class="alert alert-danger">', '</div>');
            echo $this->session->flashdata('common_flash');
            if (trim($layout) !== "") {
                echo '<div class="row white-box">';
                require_once($layout);
                echo '</div>';
            } else if (isset($this->session->designation)) {

                echo '<h1 align="center"> Welcome Again ' . $this->session->name . '</h1>';
            } else { ?>
                <?php if (config_item('is_demo') == TRUE) {
                    echo '<div class="alert alert-danger">Please Pay your software development remaining balance to remove this banner !<br/> इस बैनर को हटाने के
 लिए कृपया अपनी शेष राशि का भुगतान करें !</div>';
                } ?>
                <div class="row colorbox-group-widget">
                    <div class="col-md-3 col-sm-6 info-color-box">
                        <div class="white-box">
                            <div class="media bg-info">
                                <div class="media-body">
                                    <p class="info-text font-12"><img
                                                src="<?php echo base_url('uploads/site_img/sitemap.png') ?>"></p>
                                    <p class="info-ot font-15"><a class="btn btn-default"
                                                                  href="<?php echo site_url('tree/user-tree') ?>">View
                                            Tree
                                            &rarr;</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 info-color-box">
                        <div class="white-box">
                            <div class="media bg-primary">
                                <div class="media-body">
                                    <p class="info-text font-12"><img
                                                src="<?php echo base_url('uploads/site_img/withdraw.png') ?>"></p>
                                    <p class="info-ot font-15"><a class="btn btn-default"
                                                                  href="<?php echo site_url('income/make-payment') ?>">Pay
                                            Withdrawals
                                            &rarr;</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 info-color-box">
                        <div class="white-box">
                            <div class="media bg-primary">
                                <div class="media-body">
                                    <p class="info-text font-12"><img
                                                src="<?php echo base_url('uploads/site_img/earning.png') ?>"></p>
                                    <p class="info-ot font-15"><a class="btn btn-default"
                                                                  href="<?php echo site_url('income/view-earning') ?>">Earnings
                                            &rarr;</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 info-color-box">
                        <div class="white-box">
                            <div class="media bg-info">
                                <div class="media-body">
                                    <p class="info-text font-12"><img
                                                src="<?php echo base_url('uploads/site_img/downline.png') ?>"></p>
                                    <p class="info-ot font-15"><a class="btn btn-default"
                                                                  href="<?php echo site_url('users/view-members') ?>">Members
                                            &rarr;</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row colorbox-group-widget">
                    <div class="col-md-3 col-sm-6 info-color-box">
                        <div class="white-box">
                            <div class="media bg-primary">
                                <div class="media-body">
                                    <h3 class="info-count"><?php echo $this->db_model->count_all('member') ?> <span
                                                class="pull-right"><i
                                                    class="mdi mdi-checkbox-marked-circle-outline"></i></span></h3>
                                    <p class="info-text font-12"></p>
                                    <p class="info-ot font-15">Total Users</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 info-color-box">
                        <div class="white-box">
                            <div class="media bg-success">
                                <div class="media-body">
                                    <h3 class="info-count"><?php if ($earnnings == "") {
                                            $earnnings = 0;
                                        }
                                        echo($earnnings - 1) ?><span class="pull-right"><i
                                                    class="mdi mdi-comment-text-outline"></i></span></h3>
                                    <p class="info-text font-12"></p>
                                    <p class="info-ot font-15">Earnings</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 info-color-box">
                        <div class="white-box">
                            <div class="media bg-danger">
                                <div class="media-body">
                                    <h3 class="info-count"><?php if ($payout == "") {
                                            $payout = 0;
                                        }
                                        echo $payout ?><span class="pull-right"><i
                                                    class="mdi mdi-coin"></i></span></h3>
                                    <p class="info-text font-12"></p>
                                    <p class="info-ot font-15">Member Income</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 info-color-box">
                        <div class="white-box">
                            <div class="media bg-warning">
                                <div class="media-body">
                                    <h3 class="info-count"><?php $data = $this->db_model->sum('topup', 'member', array('join_time' => date('Y-m-d')));
                                        if ($data == "") {
                                            $data = 0;
                                        }
                                        echo $data ?><span class="pull-right"><i
                                                    class="mdi mdi-coin"></i></span></h3>
                                    <p class="info-text font-12"></p>
                                    <p class="info-ot font-15">Today's Earnings</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 5px">
                    <div class="col-md-8 col-sm-6">
                        <div id="donutchart"
                             style="width: auto; height: 400px;"></div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="white-box ie-widget m-b-0 b-b-0">
                            <h4 class="box-title">Add Company Expense</h4>
                            <div>
                                <?php echo $this->session->flashdata('other_flash') ?>
                                <?php echo form_open('admin/add_expense') ?>
                                <label>Expense Name</label>
                                <input type="text" required name="ename"
                                       class="form-control">
                                <label>Expense Amount</label>
                                <input type="text" required
                                       name="eamount"
                                       class="form-control">
                                <label>Expense Detail</label>
                                <input type="text" name="edetail"
                                       class="form-control">
                                <label>Expense Date</label>
                                <input type="text" required readonly
                                       name="edate"
                                       class="form-control datepicker">
                                <button type="submit"
                                        class="btn btn-success btn-lg pull-right glyphicon glyphicon-plus-sign"></button>
                                <?php echo form_close() ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box user-table">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h4 class="box-title">Lastest user</h4>
                                </div>
                                <div class="col-sm-6">
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>SN</th>
                                        <th>User ID</th>
                                        <th>Name</th>
                                        <th>Sponsor ID</th>
                                        <th>Phone</th>
                                        <th>Join Date</th>
                                        <th>Total Downline</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $sn = 1;
                                    foreach ($members as $e) { ?>
                                        <tr>
                                            <td><?php echo $sn++; ?></td>
                                            <td>
                                                <a href="<?php echo site_url('users/user_detail/' . $e['id']) ?>"
                                                   target="_blank"><?php echo config_item('ID_EXT') . $e['id']; ?></a>
                                            </td>
                                            <td><?php echo $e['name']; ?></td>
                                            <td>
                                                <a href="<?php echo site_url('users/user_detail/' . $e['sponsor']) ?>"
                                                   target="_blank"><?php echo $e['sponsor'] ? config_item('ID_EXT') . $e['sponsor'] : ''; ?>
                                            </td>
                                            <td><?php echo $e['phone']; ?></td>
                                            <td><?php echo $e['join_time']; ?></td>
                                            <td><?php echo($e['total_a'] + $e['total_b'] + $e['total_c'] + $e['total_d'] + $e['total_e']); ?></td>
                                            <td>
                                                <a href="<?php echo site_url('users/user_detail/' . $e['id']); ?>"
                                                   class="btn btn-warning btn-xs fa fa-search"></a><a
                                                        href="<?php echo site_url('users/edit_user/' . $e['id']); ?>"
                                                        class="btn btn-info btn-xs fa fa-pencil"></a>
                                                <a onclick="return confirm('Are you sure you want to delete this Member ?')"
                                                   href="<?php echo site_url('users/remove_member/' . $e['id']); ?>"
                                                   class="btn btn-danger btn-xs fa fa-trash"></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <a href="javascript:void(0);" class="btn btn-success pull-right m-t-10 font-20">&rarr;</a>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <!-- ===== Page-Container-End ===== -->
        <footer class="footer t-a-c">
            © <?php echo date('Y') ?> <?php echo config_item('company_name') ?>
    </div>
    <!-- ===== Page-Content-End ===== -->
</div>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url('axxets/admin/theme.js') ?>"></script>
<script src="//cdn.ckeditor.com/4.7.3/full/ckeditor.js"></script>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    CKEDITOR.replace('editor');
</script>
<script>
    $(document).ready(function () {
        $('[data-toggle="popover"]').popover({html: true, placement: "top"});
    });
</script>
<script>
    $(function () {
        $(".datepicker").datepicker({
            dateFormat: "yy-mm-dd",
            yearRange: "-70:+70",
            changeMonth: true,
            changeYear: true,
            defaultDate: 0,
            showOptions: {direction: "down"},
        });
    });
</script>
</body>
</html>
