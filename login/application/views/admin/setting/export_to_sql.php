<?php
/***************************************************************************************************
 * Copyright (c) 2017. by Haren Sarma, Exolim
 * This project is developed and maintained by Exolim IT Services Pvt Ltd.
 * Nobody is permitted to modify the source or any part of the project without permission.
 * Project Developer: Haren Sarma
 * Developed for: Exolim IT Services Pvt Ltd
 **************************************************************************************************/
?>
<div class="row">
    <div align="center">
        <div class="fa fa-database" style="font-size: 100px"></div>
        <br/>
        <p>
            <a href="<?php echo site_url('setting/export_final') ?>" class="btn btn-danger"> Export to SQL &rarr;</a>
        </p>
    </div>
</div>