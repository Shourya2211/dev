<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Management Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="//stackpath.bootstrapcdn.com/bootstrap/4.4.0/css/bootstrap.min.css" rel="stylesheet">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="//stackpath.bootstrapcdn.com/bootstrap/4.4.0/js/bootstrap.bundle.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        :root {
            --input-padding-x: 1.5rem;
            --input-padding-y: .75rem;
            }

        body {
            background: #007bff;
            background: linear-gradient(to right, #0062E6, #33AEFF);
            }

        .card-signin {
            border: 0;
            border-radius: 1rem;
            box-shadow: 0 0.5rem 1rem 0 rgba(0, 0, 0, 0.1);
            }

        .card-signin .card-title {
            margin-bottom: 2rem;
            font-weight: 300;
            font-size: 1.5rem;
            }

        .card-signin .card-body {
            padding: 2rem;
            }

        .form-signin {
            width: 100%;
            }

        .form-signin .btn {
            font-size: 80%;
            border-radius: 5rem;
            letter-spacing: .1rem;
            font-weight: bold;
            padding: 1rem;
            transition: all 0.2s;
            }

        .form-label-group {
            position: relative;
            margin-bottom: 1rem;
            }

        .form-label-group input {
            height: auto;
            border-radius: 2rem;
            }

        .form-label-group > input,
        .form-label-group > label {
            padding: var(--input-padding-y) var(--input-padding-x);
            }

        .form-label-group > label {
            position: absolute;
            top: 0;
            left: 0;
            display: block;
            width: 100%;
            margin-bottom: 0;
            /* Override default `<label>` margin */
            line-height: 1.5;
            color: #495057;
            border: 1px solid transparent;
            border-radius: .25rem;
            transition: all .1s ease-in-out;
            }

        .form-label-group input::-webkit-input-placeholder {
            color: transparent;
            }

        .form-label-group input:-ms-input-placeholder {
            color: transparent;
            }

        .form-label-group input::-ms-input-placeholder {
            color: transparent;
            }

        .form-label-group input::-moz-placeholder {
            color: transparent;
            }

        .form-label-group input::placeholder {
            color: transparent;
            }

        .form-label-group input:not(:placeholder-shown) {
            padding-top: calc(var(--input-padding-y) + var(--input-padding-y) * (2 / 3));
            padding-bottom: calc(var(--input-padding-y) / 3);
            }

        .form-label-group input:not(:placeholder-shown) ~ label {
            padding-top: calc(var(--input-padding-y) / 3);
            padding-bottom: calc(var(--input-padding-y) / 3);
            font-size: 12px;
            color: #777;
            }

        .btn-google {
            color: white;
            background-color: #ea4335;
            }

        .btn-facebook {
            color: white;
            background-color: #3b5998;
            }

        /* Fallback for Edge
        -------------------------------------------------- */

        @supports (-ms-ime-align: auto) {
            .form-label-group > label {
                display: none;
                }

            .form-label-group input::-ms-input-placeholder {
                color: #777;
                }
            }

        /* Fallback for IE
        -------------------------------------------------- */

        @media all and (-ms-high-contrast: none),
        (-ms-high-contrast: active) {
            .form-label-group > label {
                display: none;
                }

            .form-label-group input:-ms-input-placeholder {
                color: #777;
                }
            }
    </style>
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        	<?php
                echo validation_errors('<div class="alert alert-danger">', '</div>');
                echo $this->session->flashdata('common_flash');
                if (trim($layout) !== "") {
                    require_once($layout);
                } else if (isset($this->session->designation)) {

                    echo '<h1 align="center"> Welcome Again ' . $this->session->name . '</h1>';
                } else { ?>
                    <?php if (config_item('is_demo') == TRUE) {
                        echo '<div class="alert alert-danger">Please Pay your software development remaining balance to remove this banner !<br/> इस बैनर को हटाने के
 लिए कृपया अपनी शेष राशि का भुगतान करें !</div>';
                    }} ?>
            <div class="card card-signin my-5">
                <div class="card-body">
                    <h5 class="card-title text-center"><img src="<?php echo base_url('uploads/logo.png') ?>"
                                                            class="logo"
                                                            style="max-width: 200px; height: auto" alt="home"></h5>
                    <?php echo form_open('site/admin', array('class' => 'form-signin')) ?>
                    <div class="form-label-group">
                        <input autocomplete="off" type="text" id="inputEmail" name="username" class="form-control"
                               placeholder="Username" required
                        >
                        <label for="inputEmail">User Name</label>
                    </div>

                    <div class="form-label-group">
                        <input autofocus type="password" id="inputPassword" name="password" class="form-control"
                               placeholder="Password" required>
                        <label for="inputPassword">Password</label>
                    </div>

                    <div class="mb-3 text-center">
                        <label class="text-info">(Management Login)</label>
                    </div>
                    <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Sign in</button>
                    <hr class="my-4">
                    <button data-toggle="modal" data-target="#myModal"
                            class="btn btn-lg btn-google btn-block text-uppercase" type="button">Forgot Password ?
                    </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title">Reset your password</h6>
            </div>
            <div class="modal-body">
                <p>
                    <?php echo form_open('site/forgotpw_submit_admin') ?>
                <div class="form-group">
                    <label>Enter Username</label>
                    <input type="text" required class="form-control" name="user">
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit" onclick="this.innerHTML='Wait..'">Reset Password
                    </button>
                </div>
                <?php echo form_close() ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).load(function () {
        $("form").attr('autocomplete', 'off');
    })
</script>
</body>
</html>
