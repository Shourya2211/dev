<?php
/***************************************************************************************************
 * Copyright (c) 2017. by Haren Sarma, Exolim
 * This project is developed and maintained by Exolim IT Services Pvt Ltd.
 * Nobody is permitted to modify the source or any part of the project without permission.
 * Project Developer: Haren Sarma
 * Developed for: Exolim IT Services Pvt Ltd
 **************************************************************************************************/
?>
<div class="row table-responsive">
    <table class="table table-bordered">
        <thead>
        <tr>
            <td>S.N.</td>
            <td>Name</td>
            <td>Join Date</td>
            <td>Total Downline</td>
            <td>Sponsor ID</td>
        </tr>
        </thead>
        <tbody>
        <?php $this->plan_model->ref_list($this->session->user_id) ?>
        </tbody>
    </table>
</div>
