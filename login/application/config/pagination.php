<?php
/***************************************************************************************************
 * Copyright (c) 2017. by Haren Sarma, Exolim
 * This project is developed and maintained by Exolim IT Services Pvt Ltd.
 * Nobody is permitted to modify the source or any part of the project without permission.
 * Project Developer: Haren Sarma
 * Developed for: Exolim IT Services Pvt Ltd
 **************************************************************************************************/
$config['full_tag_open']   = '<ul class="pagination">';
$config['full_tag_close']  = '</ul>';
$config['first_link']      = 'First';
$config['last_link']       = 'Last';
$config['first_tag_open']  = '<li>';
$config['first_tag_close'] = '</li>';
$config['prev_link']       = '&laquo';
$config['prev_tag_open']   = '<li class="prev">';
$config['prev_tag_close']  = '</li>';
$config['next_link']       = '&raquo';
$config['next_tag_open']   = '<li>';
$config['next_tag_close']  = '</li>';
$config['last_tag_open']   = '<li>';
$config['last_tag_close']  = '</li>';
$config['cur_tag_open']    = '<li class="active"><a href="#">';
$config['cur_tag_close']   = '</a></li>';
$config['num_tag_open']    = '<li>';
$config['num_tag_close']   = '</li>';