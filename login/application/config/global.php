<?php
defined('BASEPATH') OR exit('Can we play bubu together ?');

$config['company_name'] = "Smart Money";
$config['company_address'] = "";
$config['ID_EXT'] = 'M'; # ID Extension eg: DM1001
$config['currency'] = "Rs.  "; # Sitewide currency
$config['iso_currency'] = "INR  "; # ISO Code of currency
$config['leg'] = "1"; # 1, 2, 3, 4, 5(eg: for binary plan leg is 2)
$config['show_leg_choose'] = "Yes"; ## Whether to show placement ID box at registration
$config['show_placement_id'] = "Yes"; ## Whether to show select position option or not
$config['autopool_registration'] = "No";
$config['show_join_product'] = "Yes";
$config['enable_epin'] = "Yes";
$config['enable_pg'] = "No"; # Payment Gateway
$config['free_registration'] = "No";
$config['sms_on_join'] = "Yes";
$config['top_id'] = "1001";
$config['prevent_join_product_entry'] = "No";
$config['enable_gap_commission'] = "No";
$config['sms_api'] = "https://api.msg91.com/api/sendhttp.php?sender=HITSIN&route=4&mobiles={{phone}}&authkey=232021ABc3W1cU5b7527e3&country=91&message={{msg}}";
## Format: https://apiurl.com?no={{phone}}&msg={{msg}}&other_parameters.
$config['disable_registration'] = "No";
$config['fix_income'] = "No";
$config['give_income_on_topup'] = "Yes";
####################### MODULE SETTING ##############################
$config['enable_topup'] = "Yes";
$config['enable_repurchase'] = "No";
$config['enable_coupon'] = "No";
$config['enable_ad_incm'] = "No";
$config['enable_survey'] = "No";
$config['enable_recharge'] = "No";
$config['enable_reward'] = "Yes";
$config['enable_help_plan'] = "No";
$config['enable_product'] = "Yes";
$config['investment_mode'] = "AUTO"; ## AUTO, EPIN, MANUAL
$config['enable_investment'] = "No"; ## This will convert existing software to a investment plan software and will turn off many features.

