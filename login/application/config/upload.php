<?php
/***************************************************************************************************
 * Copyright (c) 2017. by Haren Sarma, Exolim
 * This project is developed and maintained by Exolim IT Services Pvt Ltd.
 * Nobody is permitted to modify the source or any part of the project without permission.
 * Project Developer: Haren Sarma
 * Developed for: Exolim IT Services Pvt Ltd
 **************************************************************************************************/
$config['upload_path']      = './uploads/';
$config['allowed_types']    = 'gif|jpg|png|PNG|jpeg|pdf|tiff|docx';
$config['file_ext_tolower'] = TRUE;
$config['encrypt_name']     = TRUE;
$config['max_filename']     = '255';
